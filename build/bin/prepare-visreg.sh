#!/bin/sh
SRC_BRANCH=${BITBUCKET_BRANCH}
DEST_BRANCH=${BITBUCKET_PR_DESTINATION_BRANCH}

if [ "x" = "x${DEST_BRANCH}" ]; then
    # we aren't running in a pull request.
    echo "No discoverable destination branch, assuming self."
    DEST_BRANCH="${SRC_BRANCH}"
fi

git clone git@bitbucket.org:atlassian/aui-visual-regression.git visreg
cd visreg

# check if the destination branch exists in our visreg images
git ls-remote --exit-code --heads origin "${DEST_BRANCH}"

if [ $? -eq 0 ]; then
    echo "Checking out visual regression images for ${DEST_BRANCH}..."
    git checkout ${DEST_BRANCH}
    exit 0
else
    # no matching branch
    echo "No visual regression images recorded for ${DEST_BRANCH}..."
    exit 1
fi
