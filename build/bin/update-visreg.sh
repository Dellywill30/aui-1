#!/bin/sh
cd visreg
ANYTHING_CHANGED=$(git status --porcelain)
CHANGE_COUNT=$(echo "$ANYTHING_CHANGED" | wc -l)

if [ "x" = "x${ANYTHING_CHANGED}" ]; then
    echo "Nothing changed; all good!"
    exit 0
else
    echo "There are some changes! Time to git busy."
    TO_BRANCH="sandbox/${BITBUCKET_BUILD_NUMBER}-${BITBUCKET_PIPELINE_UUID}"
    cat <<EOF > commitmsg.txt
New screenshots added from ${BITBUCKET_BUILD_NUMBER}

Based off commit: ${BITBUCKET_GIT_HTTP_ORIGIN}/commits/${BITBUCKET_COMMIT}
Pipeline build: ${BITBUCKET_GIT_HTTP_ORIGIN}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}
Pipeline ID: ${BITBUCKET_PIPELINE_UUID}
EOF
    # identify ourselves in git
    git config user.name "auipipelines"
    git config user.email "build-agent@atlassian.com"
    # set the upstream. You can't push via SSH in pipelines, but you can via HTTPS.
    git remote set-url origin "https://${VISREG_USERNAME}:${VISREG_SECRET}@bitbucket.org/atlassian/aui-visual-regression.git"
    git remote -v
    #
    # create the commit contents
    # NOTE: this part is coupled to our visreg runners and the results they produce.
    #       see `<projectRoot>/cypress/integration/visreg`
    #       see the `visreg/x/ci` script commands in `package.json`
    #
    git checkout -q -b ${TO_BRANCH}
    git add *.png
    git commit --file=commitmsg.txt
    # push our changes to the visreg repo
    git push origin ${TO_BRANCH}
    echo "Pushed some screenshots to ${TO_BRANCH}; check that out and merge them if they're the new baseline."
    #
    # if this was triggered in a PR, we'll do some extra work to make the flow easier.
    #
    if [ "x" != "x${BITBUCKET_PR_ID}" ]; then
      # raise a PR, then capture the HREF for visiting it in a browser
      result=$(curl -v https://api.bitbucket.org/2.0/repositories/atlassian/aui-visual-regression/pullrequests \
        -u "${VISREG_USERNAME}:${VISREG_SECRET}" \
        --request POST \
        --header 'Content-Type: application/json' \
        --data "{
          \"title\": \"New screenshots added from ${BITBUCKET_BUILD_NUMBER}\",
          \"description\": \"Merge it only if the corresponding AUI PR is going to be merged: https://bitbucket.org/atlassian/aui/pull-requests/${BITBUCKET_PR_ID}\",
          \"source\": {
            \"branch\": {
              \"name\": \"${TO_BRANCH}\"
            }
          },
          \"destination\": {
            \"branch\": {
              \"name\": \"${BITBUCKET_PR_DESTINATION_BRANCH}\"
            }
          }
        }")
      result_pr_id=$(echo $result| jq --raw-output '.id')
      result_href=$(echo $result| jq --raw-output '.links.html.href')
      visreg_ui_href="https://aui-visreg-ui.netlify.app/?pr=${result_pr_id}"
      # add a comment to this PR to link over to the visreg PR
      curl -v https://api.bitbucket.org/2.0/repositories/atlassian/aui/pullrequests/${BITBUCKET_PR_ID}/comments \
        -u "${VISREG_USERNAME}:${VISREG_SECRET}" \
        --request POST \
        --header 'Content-Type: application/json' \
        --data "{
          \"content\": {
            \"raw\": \"There are ${CHANGE_COUNT} visual changes to review.\n\n* Review changes at ${visreg_ui_href} \n* PR raised at ${result_href}\"
          }
        }"
    fi
    exit 1
fi
