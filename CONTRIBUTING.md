# Developing AUI

## Requirements

Building and working with AUI's source requires at minimum:

- Node 12+
- yarn 1.17+
- Java 1.8+ - for building the soy templates.

AUI has optional dependencies for building parts of it:

- Maven 3.5.4 - for building the P2 plugin and its test harnesses.
- docker-machine 0.13+ - for running various tests in a container.

## Development environment

First, install all the required dependencies:

```
yarn
```
(This is the shorthand for `yarn install`)

This repository uses `yarn` Workspaces.

To get familiar with `yarn` visit: https://classic.yarnpkg.com/lang/en/ (this is for the version 1 of yarn; yarn 2 is not supported)
### Available scripts

To see a list of available scripts run:

```
yarn run
```

or read through the `package.json` file. Each package from `./packages` directory may have their own set of scripts. Below you can find a list of all scripts available from the root directory:

- `clean` - removes all build artefacts
- `a11y/docs` - runs accessibility tests on AUI documentation pages (requires local server running on port 8000 with docs website)
- `a11y/docs/ci` - runs accessibility tests on AUI docs on CI
- `a11y/flatapp` - runs accessibility tests on AUI flatapp
- `a11y/flatapp/ci` - runs accessibility tests on AUI flatapp on CI
- `dist` - creates production build for npm
- `dist-legacy` - creates a build for legacy browsers **DEPRECATED**
- `dist-modern` - creates a build for modern browsers **DEPRECATED**
- `dist/build` - **partial** used by `dist`
- `bundlesize/verify-dist` - checks bundle sizes for dist files against a strict threshold
- `bundlesize/raw` - checks bundle size of unminified files
- `bundlesize/gzipped` - checks bundle size of gzipped files
- `docs` - creates a documentation build and prepares for deployment
- `docs/build` - **partial** used by `docs` to build static pages
- `docs/run` - runs documentation pages locally
- `docs/watch` - runs documentation pages locally in watch mode (you have to refresh browser manually, but the files are compiled on every change)
- `e2e` - runs cypress e2e tests **TODO: needs review**
- `flatapp` - builds the AUI flatapp and prepares it for deployment
- `flatapp/build` - builds AUI flatapp,
- `flatapp/run` - runs AUI flatapp locally
- `flatapp/watch` - runs AUI flatapp locally in watch mode
- `icons/build` - builds icon fonts
- `lint` - statically analyses the code for common errors and bad patterns
- `p2-plugin` - creates production build of p2-plugin
- `p2-plugin/build` - builds dev version of p2-plugin
- `p2-plugin/run` - runs production version of p2-plugin locally
- `p2-plugin/watch` - runs development version of p2-plugin locally
- `postinstall` - applies patches to node_modules
- `pre-commit-lint` - runs `lint` on staged files
- `pre-commit` - runs pre-commit code checks
- `prep-deploy/docs` - prepares docs for deployment
- `prep-deploy/flatapp` - prepares flatapp for deployment
- `refapp` - runs refapp locally
- `refapp/run` - just an alias for `refapp` for consistency
- `setuphooks` - sets up git hooks
- `tasks` - **TODO: unknown purpose, should be deprecated probably**
- `test` - runs unit and integration tests with Karma in the browser
- `test/watch` - runs tests in watch mode
- `test-docker/chrome` - runs tests locally in Chrome on docker **DEPRECATED: we use browserstack**
- `test-docker/firefox` - runs tests locally in Firefox on docker **DEPRECATED: we use browserstack**
- `visreg/docs` - runs visual regression tests on documentation pages
- `visreg/docs/ci` - just `visreg/docs` for CI
- `visreg/refapp` - runs visual regression tests on refapp pages
- `visreg/refapp/ci` - just `visreg/refapp` for CI,
- `visreg/refapp/open` - runs visual regression tests in Chrome, with debugging tools, locally
- `visreg/flatapp` - runs visual regression tests on flatapp pages locally
- `visreg/flatapp/ci` - just `visreg/flatapp` for CI
- `visreg/flatapp/docker/chrome` - runs visual regression tests for flatapp locally in Chrome on docker

### Git hooks

To set up git hooks run `yarn setuphooks`.

> **TODO** What kind of hooks do we have?

## AUI's deliverable assets

AUI is a collection of UI components written in JavaScript, CSS, and HTML.

AUI is shipped in two ways:

* As a Node package (or "dist" for short) - for standalone use.
* As an Atlassian P2 plugin (or "p2-plugin" for short) - for usage with Atlassian's products.

### Browser support

We have a `browserslist` section in the root `package.json` of the AUI monorepo
that lists all of our supported browsers in production.

### Building

These are the most common commands to build the key deliverable assets of AUI:

#### Distribution build for npm

To build AUI as a set of concatenated files appropriate for using in a browser:

```
yarn dist 
```        

#### Distribution build for p2-plugin

To compile the AUI source as an Atlassian P2 Plugin. **NOTE:** requires Maven to be available.

```
yarn p2-plugin  
```    

#### Documentation static pages

To build the AUI documentation.

```
yarn docs 
```           

## Testing

We encourage that you write automated tests for your change before writing its production code.

This approach leads to more modular APIs and will increase maintainers' confidence in making future changes to the library.

You can test your changes in a few ways.

### Integration tests

The bulk of AUI's tests are integration tests - they exercise each component in a real browser.

Tests are written using the [`mocha` test runner](https://mochajs.org/#run-cycle-overview).

Assertions are written using [`chai` and its `expect()` interface](https://www.chaijs.com/guide/styles/#expect).

We use [Karma](https://karma-runner.github.io) to run all tests.

To run tests once:

    yarn test

To run the tests in watch mode:

    yarn test/watch

Both scripts accept the same arguments:

* `--browsers [Chrome,Firefox]` --
  The browsers to run the tests in.
  Valid values are `Chrome`, `Firefox`, `Opera` and `Safari`.

* `--grep [pattern]` --
  Specify a pattern to match a subset of tests to run.

*It's recommended to just run a single describe block in TDD mode, use [`describe.only`](https://mochajs.org/#exclusive-tests)*

### Integration tests via BrowserStack

You can run the integration tests via BrowserStack.

Add a `BROWSERSTACK_USER` and `BROWSERSTACK_KEY` environment variable to your local development environment.
Their values should be set to your personal username and access key listed on
[Browserstack's account settings page](https://www.browserstack.com/accounts/settings).

### Integration tests via Docker

Alternatively, you can run the Chrome and Firefox unit tests via `yarn test-docker/chrome` or `yarn test-docker/firefox`.

### Acceptance testing

We have a set of test pages that help stress-test the AUI components and patterns during development and acceptance testing.
There are two ways to run the reference application:

#### A static site (aka the "flatapp")

Running AUI and its test pages as a set of static HTML, CSS, and JavaScript files is the fastest way to use them.

To run the application in a production-like environment, use:

    yarn flatapp/run

By default, this will open up a page at [http://127.0.0.1:7000/pages/](http://127.0.0.1:7000/pages/).
You can configure where the server will run:

* `--host [127.0.0.1]` -- The host to start the server on.
* `--port [7000]` -- The port to start the server on.

#### An Atlassian P2 plugin

The test pages can be run inside the Atlassian reference application plugin environment (aka the "refapp").
This is slower, but the closest analog to how AUI is consumed by Atlassian products.

To run the application in a production-like environment, use:

    yarn p2-plugin/run

By default, this will run the Atlassian plugin at [http://127.0.0.1:9999/ajs/](http://127.0.0.1:9999/ajs/).

### Visual regression testing

We have a set of npm script dedicated to run on CI (Bamboo). They are prefixed with `visreg` and you do not have to run
them manually. If you need to add more of those and check if they work you can run: `yarn visreg/flatapp/docker/chrome`
(which runs on Docker) or `yarn visreg/flatapp/ci` if it is fine for you to run it locally.

We do not commit resulting artifacts to the repo - the CI job does that by creating new branch on
`https://bitbucket.org/atlassian/aui-visual-regression` (Only in case of new screenshots generated). Just remember to
make a PR out of it.

## Documenting

All the documentation on [https://aui.atlassian.com] is developed and released from this repository.

To run the application in a production-like environment, use:

    yarn docs/run

By default this will open up a page at [http://127.0.0.1:8000/](http://127.0.0.1:8000/).
You can configure where the server will run:

* `--host [127.0.0.1]` -- The host to start the server on.
* `--port [8000]` -- The port to start the server on.

## Developing

The documentation and reference applications can all be built and watched for changes:

    yarn docs/watch
    yarn flatapp/watch
    yarn p2-plugin/watch

Each accepts the same parameters as their `*/run` counterparts.

### CSS toggles

We use `.aui-legacy-x` for all our CSS feature flag toggles.

### Bundle size checks

We have the following commands for verifying the size of our distribution build:

    # first, build the distribution
    yarn dist

    # then, run one of these commands.
    # each will check the the sizes of files generated from the `yarn dist` command
    # and verify they aren't getting larger than the allowed limits.
    # 'raw' is the output size when minified; 'gzipped' is the size when min+gz.
    yarn bundlesize/raw
    yarn bundlesize/gzipped

#### Comparative bundle sizes

You can compare how the size of the distribution build changes if browser support is changed.
To do this, run `yarn dist-legacy` or `yarn dist-modern` instead of `yarn dist`, then
run the checks as per normal.

#### Updating the bundle sizes

The bundle sizes are stored in `packages/core/scripts/dependencies/files-with-sizes.js`.

Updating the bundle sizes should only be considered after attempts to reduce the overall
library size have been made.

Every kilobyte saved matters to our end-users! Help keep our apps fast by keeping our library small.

## Contributing

When contributing to AUI, we ask that you follow these steps.

1. Create an issue in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

   Creating an issue is a good place to talk with the AUI team about whether anyone else is
   working on the same issue, what the best fix is, and if this is a new feature, whether it belongs in AUI.

   If you don't create an issue, we'll ask you to create one when you issue the PR and re-tag your
   commits with the issue key.

2. Create a branch for your change.

    a. If your change is meant for a specific version of AUI, start your branch against the appropriate
       version branch of AUI. For example, if you are attempting to fix a bug in AUI 6.0.8, start your
       branch from the `6.0.x` branch of AUI.

    b. Name your branch in the format `{issue-key}-{description}` -- for example,
       `AUI-1337-fix-the-contributor-guide`.

3. Make your changes.

    a. Ensure all your commits are tagged with the issue key, ideally at the start of the commit message --
       for example, `AUI-1337 - fixes to contributor guide`.

    b. Write tests! Automated tests are preferred over manual acceptance tests.

    c. Write documentation for your change! Updates that include changelog entries or code
       examples are a huge plus ;)

4. Push your branch and create a pull request.

    a. You should fork the AUI repository, and push your branch to your fork.

    b. When creating your PR, target the `master` branch.

    c. In your PR description, write one sentence to answer each of these questions:

        * What this change helps solve, and for whom?
        * What kind of advice or feedback, if any, do you want on the change?
        * Is this change needed in an earlier version of the library? If so, why?

       Answering these questions helps to set clear expectations, which will expedite the review and release process.

5. PR review and acceptance.

    a. Please do follow up with us if your PR doesn't receive attention after a few days.
       It's okay to ask for the maintainers' attention!

    b. By default, PR comments are suggestions, not mandates. You may choose to take the
       advice or not. Reply to indicate whether you will the make suggested changes or not.

    c. PR comments with tasks attached are considered required. You must complete the task
       in order for the PR to be accepted.

    d. Once all PR tasks are resolved and all builds are green, the PR can be merged!
       One of the maintainers with commit rights will merge your changes and indicate when
       to expect the next release with the change in it.

## PR checklist

### Documentation

- Does this need a changelog entry?
- Does it need new docs?
- Do the current docs need to be updated?
- Will this deprecate anything? Is that thing marked as deprecated (in code and in docs)?

### Testing

- Does it needs unit tests?
- Were the proper unit tests written?
- Do the tests pass?
- Does it need test-page examples?
- Does the test page work in both Refapp and Flatapp?
- Does it work in all browsers enlisted in `browserlist` section of `package.json` file?

### Commits

- Is the branch up to date with master or given release branch?

### Code hygiene

- Are all strings internationalised?
- Does it adhere to code quality guidelines? (jslint)

### Code location

- What is the earliest version that it affects?

### Accessibility

- Does this need an accessibility audit? Is there an issue for that?