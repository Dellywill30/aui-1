import '@atlassian/aui/src/js/aui/tooltip';
import $ from '@atlassian/aui/src/js/aui/jquery';
import { hover, mockPopper } from '../../helpers/all';

const TOOLTIP_SELECTOR = '#aui-tooltip';
const TOOLTIP_TIMEOUT = 501;

export function removeTooltipContainer() {
    $(TOOLTIP_SELECTOR).remove();
}

export function getTooltipContent () {
    return document.querySelector('.aui-tooltip-content');
}

describe('aui/tooltip', () => {
    let ctx = document.body;
    let $tooltipTrigger;
    let popperMock;

    beforeEach(() => {
        popperMock = mockPopper();
    })

    afterEach(() => {
        $tooltipTrigger.tooltip('destroy');
        $tooltipTrigger.remove();
        popperMock.restore();
    });

    /**
     * @returns {boolean}
     */
    function isTooltipVisible() {
        const tooltipContainer = getTooltipContainer();

        return !!tooltipContainer.attr('style') && !!tooltipContainer.attr('data-popper-placement');
    }

    function getTooltipContainer() {
        return $(ctx).find(TOOLTIP_SELECTOR);
    }

    function createTooltip(tooltipOptions) {
        $tooltipTrigger = $('<div title="The tooltip" style="margin: 200px">Element<div>');
        $(ctx).find('#test-fixture').append($tooltipTrigger);
        $tooltipTrigger.tooltip(tooltipOptions);
    }

    function focusOnTooltipTrigger() {
        hover($tooltipTrigger);
        popperMock.tick(TOOLTIP_TIMEOUT);
    }

    it('should create tooltip container in given context', () => {
        createTooltip();

        focusOnTooltipTrigger();

        expect(getTooltipContainer()[0]).to.exist;
    });

    it('should be destroyed with the "destroy" option', () => {
        createTooltip();
        focusOnTooltipTrigger();

        $tooltipTrigger.tooltip('destroy');

        expect(isTooltipVisible()).to.equal(false);
    });

    it('should throw error when command name does not exist', () => {
        createTooltip();
        focusOnTooltipTrigger();

        expect(() => $tooltipTrigger.tooltip('non-existing-cmd-name'))
            .to.throw('Method non-existing-cmd-name does not exist on tooltip.');
    });

    it('should show tooltip with "hide" option', () => {
        createTooltip();
        focusOnTooltipTrigger();

        $tooltipTrigger.tooltip('hide');

        expect(isTooltipVisible()).to.equal(false);
    });

    it('should be visible', () => {
        createTooltip();

        focusOnTooltipTrigger();

        expect(isTooltipVisible()).to.equal(true);
    });

    describe('should contain', () => {
        [
            { gravity: 'n', expectedPopperPlacement: 'bottom' },
            { gravity: 'ne', expectedPopperPlacement: 'bottom-end' },
            { gravity: 'e', expectedPopperPlacement: 'left' },
            { gravity: 'se', expectedPopperPlacement: 'top-end' },
            { gravity: 's', expectedPopperPlacement: 'top' },
            { gravity: 'sw', expectedPopperPlacement: 'top-start' },
            { gravity: 'w', expectedPopperPlacement: 'right' },
            { gravity: 'nw', expectedPopperPlacement: 'bottom-start' },
        ].forEach(({ gravity, expectedPopperPlacement }) => {
            it(`popper placement "${expectedPopperPlacement}" for gravity "${gravity}"`, () => {
                createTooltip({ gravity });

                focusOnTooltipTrigger();

                expect(getTooltipContainer().attr('data-popper-placement')).to.equal(expectedPopperPlacement);
            });
        });
    });

    describe('live tooltip', () => {
        function createLiveTooltip() {
            $tooltipTrigger = $('<div class="tooltip-me" title="Tooltip on dynamic element">I was added dynamically!</div>');
            $(ctx).tooltip({
                live: '.tooltip-me'
            });
            $(ctx).find('#test-fixture').append($tooltipTrigger);
        }

        it('should be visible', () => {
            createLiveTooltip();

            focusOnTooltipTrigger();

            expect(isTooltipVisible()).to.equal(true);
        });

        it('should be destroyed', () => {
            createLiveTooltip();
            focusOnTooltipTrigger();

            $tooltipTrigger.tooltip('destroy');

            expect(isTooltipVisible()).to.equal(false);
        });
    });
});
