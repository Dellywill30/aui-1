import $ from '@atlassian/aui/src/js/aui/jquery';
import banner from '@atlassian/aui/src/js/aui/banner';

describe('aui/banner', function () {
    it('cannot be created unless a #header is present', function () {
        try {
            expect(banner({body: 'test'})).to.throw(Error);
        } catch (e) {
            expect(e.message && e.message.indexOf('header')).to.be.above(-1);
        }
    });

    describe('API', function () {
        var header;

        beforeEach(function () {
            header = $('<div id="header" role="banner"></div>');
            header.html('<p>test</p><nav class="aui-header"></nav>');
            $('#test-fixture').append(header);
        });

        it('global', function () {
            expect(AJS.banner.toString()).to.equal(banner.toString());
        });

        it('AMD module', function (done) {
            amdRequire(['aui/banner'], function (amdModule) {
                expect(amdModule).to.equal(banner);
                done();
            });
        });

        it('should return a DOM element', function () {
            var b = banner({body: 'test banner'});
            expect(b).to.be.an.instanceof(HTMLElement);
        });

        it('are prepended to the header', function () {
            var b = banner({body: 'test banner'});
            expect(header.children().length).to.equal(3);
            expect(header.children().get(0)).to.equal(b);
        });

        it('can have a body that accepts html', function () {
            var b = banner({body: 'with an <strong>important <a href="#">hyperlink</a></strong>!'});
            expect(b.textContent, 'with an important hyperlink!');
            expect(b.querySelectorAll('strong').length).to.equal(1);
            expect(b.querySelectorAll('a').length).to.equal(1);
        });
    });

    describe('security', function() {
        let alerter;
        beforeEach(function () {
            alerter = sinon.stub(window, 'alert');
            $('#test-fixture').html('<div id="header" role="banner"><nav class="aui-header"></nav></div>');
        });
        afterEach(function () {
            alerter.restore();
        });

        it('does not execute scripts', function() {
            banner({body: 'this script should not execute <script>alert("whoops")</script>'});
            expect(alerter.callCount).to.equal(0);
        });
    });
});
