import globalize from '@atlassian/aui/src/js/aui/internal/globalize';

describe('aui/internal/globalize', function () {
    var oldAjs;

    beforeEach(function () {
        oldAjs = window.AJS;
        window.AJS = undefined;
    });

    afterEach(function () {
        window.AJS = oldAjs;
    });

    describe('the AJS namespace', function() {
        it('should be created if it does not exist', function () {
            expect(window.AJS).to.be.undefined;
            globalize('test', true);
            expect(window.AJS).to.not.be.undefined;
        });

        it('should be an object when created', function () {
            globalize('test', true);
            expect(window.AJS).to.be.an('object');
        });

        it('should leave any previously-created object in place', function () {
            var ourAjs = window.AJS = { foo: 'bar' };
            globalize('test', true);
            expect(window.AJS).to.equal(ourAjs);
        });

        it('should get a new property based on the name and value passed in', function () {
            globalize('test', true);
            expect(window.AJS.test).to.equal(true);
        });
    });

    it('should return the namespaced value that was passed in', function () {
        expect(globalize('test', true)).to.equal(true);
    });
});
