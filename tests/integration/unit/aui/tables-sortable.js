import tablessortable from '@atlassian/aui/src/js/aui/tables-sortable';
import $ from 'jquery';
import {
    fixtures,
    afterMutations
} from '../../helpers/all';

describe('aui/tables-sortable', function () {
    it('globals', function () {
        expect(AJS.tablessortable.toString()).to.equal(tablessortable.toString());
    });
    describe('intialization', function () {
        describe(' - with imperative API', function () {
            let table;
            beforeEach(function () {
                const constructed = fixtures({
                    table: `<table class="aui aui-table-sortable test-table">
                            <thead>
                                <tr><th>Test</th></tr>
                            </thead>
                            <tbody>
                                <tr><td>1</td></tr>
                                <tr><td>2</td></tr>
                            </tbody>
                        </table>`
                });
                table = constructed.table;
            });

            it('- should initialize', function () {
                tablessortable.setTableSortable($('.test-table'));
                expect(table.classList.contains('tablesorter')).to.be.true;
            });

            it('- should initialize with events', function () {
                const eventSpy = sinon.spy();
                const $table = $('.test-table');
                tablessortable
                    .setTableSortable($table)
                    .on('sortStart', eventSpy)
                    .on('sortStart', eventSpy)
                    .on('sortStart', eventSpy)

                $table.find('th:eq(0)').trigger('sort');
                expect(eventSpy.callCount).to.equal(3);
            });

            it('- should initialize with server-side sorting options', function () {
                const $table = $('.test-table');
                const $th = $table.find('th:eq(0)');
                const clock = sinon.useFakeTimers();
                tablessortable
                    .setTableSortable($table, {
                        serverSideSorting: true,
                        appender: (target, rows) => {
                            const $target = $(target);
                            const sort = $target.find('th:eq(0)').attr('aria-sort');
                            setTimeout(() => {
                                rows.sort(($a, $b) => {
                                    const a = parseInt($a.text());
                                    const b = parseInt($b.text());
                                    return sort === 'ascending' ? a - b : b - a;
                                });

                                $target.find('tbody').html(rows);
                            }, 100)
                        }
                    });

                $th.trigger('sort');
                clock.tick(150);
                $th.trigger('sort');
                clock.tick(150);

                afterMutations(() => {
                    expect($th.attr('aria-sort')).to.equal('descending');
                    expect($table.find('tbody tr').eq(0).text()).to.equal('2');
                });
            });

            it('- should re-initialize with new options', function () {
                const $table = $('.test-table');
                const $th = $table.find('th:eq(0)');
                tablessortable.setTableSortable($table);
                $table.trigger('destroy');
                tablessortable.setTableSortable($table, {
                    serverSideSorting: true,
                });

                $th.trigger('sort').trigger('sort');

                afterMutations(() => {
                    expect($th.attr('aria-sort')).to.equal('descending');
                    expect($table.find('tbody tr').eq(0).text()).to.equal('1');
                });
            });
        });
    });

    describe('methods', function () {
        beforeEach(function () {
            fixtures({
                table: `<table class="aui aui-table-sortable test-table">
                            <thead>
                                <tr><th>Test</th></tr>
                            </thead>
                            <tbody>
                                <tr><td>1</td></tr>
                                <tr><td>2</td></tr>
                            </tbody>
                        </table>`
            });
        });

        it('- allows to sort', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            tablessortable.setTableSortable($table);

            $th.trigger('sort').trigger('sort');

            afterMutations(() => {
                expect($th.attr('aria-sort')).to.equal('descending');
                expect($table.find('tbody tr').eq(0).text()).to.equal('2');
            });
        });

        it('- allows to destroy', () => {
            const $table = $('.test-table');
            tablessortable.setTableSortable($table);
            $table.trigger('destroy');

            afterMutations(() => {
                expect($table.hasClass('tablesortable')).to.be.false;
            });
        });
    });

    describe('events', function () {
        beforeEach(function () {
            fixtures({
                table: `<table class="aui aui-table-sortable test-table">
                            <thead>
                                <tr><th>Test</th></tr>
                            </thead>
                            <tbody>
                                <tr><td>1</td></tr>
                                <tr><td>2</td></tr>
                            </tbody>
                        </table>`
            });
        });

        it('- listens to sortBegin event', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            const eventSpy = sinon.spy();
            tablessortable.setTableSortable($table).on('sortBegin', eventSpy);

            $th.trigger('sort');

            afterMutations(() => {
                expect(eventSpy).to.be.calledOnce;
            });
        });

        it('- listens to sortStart event', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            const eventSpy = sinon.spy();
            tablessortable.setTableSortable($table).on('sortStart', eventSpy);

            $th.trigger('sort');

            afterMutations(() => {
                expect(eventSpy).to.be.calledOnce;
            });
        });

        it('- listens to sortEnd event', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            const eventSpy = sinon.spy();
            tablessortable.setTableSortable($table).on('sortEnd', eventSpy);

            $th.trigger('sort');

            afterMutations(() => {
                expect(eventSpy).to.be.calledOnce;
            });
        });
    });

    describe('options', function () {
        beforeEach(function () {
            fixtures({
                table: `<table class="aui aui-table-sortable test-table">
                            <thead>
                                <tr><th>Test</th></tr>
                            </thead>
                            <tbody>
                                <tr><td>1</td></tr>
                                <tr><td>2</td></tr>
                            </tbody>
                        </table>`
            });
        });

        it('- allows server-side sorting and disabling client-side sorting', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            tablessortable.setTableSortable($table, {
                serverSideSorting: true
            });

            $th.trigger('sort').trigger('sort');

            afterMutations(() => {
                expect($th.attr('aria-sort')).to.equal('descending');
                expect($table.find('tbody tr').eq(0).text()).to.equal('1');
            });
        });

        it('- allows custom appender', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            tablessortable.setTableSortable($table, {
                appender: (target, rows) => {
                    const $target = $(target);
                    const sort = $target.find('th:eq(0)').attr('aria-sort');
                    rows.forEach(($el) => {
                        $el.attr('data-sort', sort);
                    });
                    $target.find('tbody').html(rows);
                }
            });

            $th.trigger('sort');

            afterMutations(() => {
                expect($th.attr('aria-sort')).to.equal('ascending');
                expect($table.find('tbody tr').eq(0).data('sort')).to.equal('ascending');
            });
        });

        it('- allows to reset sorting on third click', () => {
            const $table = $('.test-table');
            const $th = $table.find('th:eq(0)');
            tablessortable.setTableSortable($table, {
                sortReset: true
            });

            $th
                .trigger('sort')
                .trigger('sort')
                .trigger('sort');

            afterMutations(() => {
                expect($th.attr('aria-sort')).to.equal('none');
            });
        });
    });
});
