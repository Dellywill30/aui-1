import '@atlassian/aui/src/js/aui/trigger';
import $ from '@atlassian/aui/src/js/aui/jquery';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import {
    afterMutations,
    blur,
    click,
    focus,
    hover,
} from '../../helpers/all';

describe('aui/trigger', function () {
    var element;
    var trigger;
    var disabledTrigger;
    var clock;
    var componentAttributeFlag = 'data-my-component';
    var componentId = 'my-element';

    before(function () {
        createComponent();
    });

    function createComponent () {
        return skate(componentAttributeFlag, {
            type: skate.type.ATTRIBUTE,
            prototype: {
                show: function () {
                    this.style.display = 'block';
                    this.dispatchEvent(new CustomEvent('aui-after-show'));
                },
                hide: function () {
                    this.style.display = 'none';
                    this.dispatchEvent(new CustomEvent('aui-after-hide'));
                },
                isVisible: function () {
                    return this.style.display === 'block';
                }
            }
        });
    }

    function createElement () {
        var el = document.createElement('div');
        el.setAttribute('hidden', '');
        $(el)
            .text('some content')
            .attr('id', componentId)
            .attr(componentAttributeFlag, '')
            .css({
                display: 'none',
                height: 100,
                width: 100
            })
            .appendTo('#test-fixture');
        skate.init(el);

        return el;
    }

    function triggerFactory (tag, attributes, innerHTML) {
        var el = document.createElement(tag);
        if (typeof innerHTML === 'string') {
            el.innerHTML = innerHTML;
        }

        if (attributes && typeof attributes === 'object') {
            for (var prop in attributes) {
                if (attributes.hasOwnProperty(prop)) {
                    el.setAttribute(prop, attributes[prop]);
                }
            }
        }

        el.setAttribute('data-aui-trigger', '');
        el.setAttribute('aria-controls', componentId);
        document.getElementById('test-fixture').appendChild(el);

        skate.init(el);

        return el;
    }

    function createButtonTrigger (attributes, innerHTML) {
        return triggerFactory('button', attributes, innerHTML);
    }

    function createAnchorTrigger (attributes, innerHTML) {
        return triggerFactory('a', attributes, innerHTML);
    }

    function disableTrigger (trigger) {
        trigger.setAttribute('aria-disabled', 'true');
        return trigger;
    }

    it('global', function () {
        // Placeholder for future global. Currently, we cannot export this as
        // "trigger" because a legacy funciton of the same name already exists
        // that does something completely different.
    });

    it('AMD module', function (done) {
        amdRequire(['aui/trigger'], function (amdModule) {
            expect(amdModule).to.equal(undefined);
            done();
        });
    });

    describe('Behaviour -', function () {
        beforeEach(function () {
            element = createElement();

            element.message = sinon.spy();

            trigger = createButtonTrigger();
            disabledTrigger = disableTrigger(createButtonTrigger());
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        describe('on prototype -', function () {
            it('isEnabled() should return false after aria-disabled="true" is added', function () {
                expect(trigger.isEnabled()).to.be.true;
                disableTrigger(trigger);
                expect(trigger.isEnabled()).to.be.false;
            });

            it('isEnabled() should return true when there is no aria-disabled attribute', function () {
                expect(trigger.isEnabled()).to.be.true;
            });

            it('disable() should disable the trigger', function () {
                expect(trigger.isEnabled()).to.be.true;
                trigger.disable();
                expect(trigger.isEnabled()).to.be.false;
            });

            it('enable() should enable the trigger', function () {
                expect(trigger.isEnabled()).to.be.true;
                trigger.disable();
                expect(trigger.isEnabled()).to.be.false;
                trigger.enable();
                expect(trigger.isEnabled()).to.be.true;
            });
        });

        it('component should receive click message when trigger is clicked', function () {
            click(trigger);
            expect(element.message).to.have.been.calledOnce;
            expect(element.message).to.have.been.calledWith(sinon.match.has('type', 'click'));
        });

        it('component should receive hover message when trigger is hovered', function () {
            hover(trigger);
            expect(element.message).to.have.been.calledOnce;
            expect(element.message).to.have.been.calledWith(sinon.match.has('type', 'mouseenter'));
        });

        it('component should receive focus message when trigger is focused', function () {
            focus(trigger);
            // Caveat: will be called twice on non-IE,
            // due to both native and custom event being fired
            // in order to not introduce browser sniffing into tests, we allow any number of calls
            expect(element.message).to.have.been.called;
            expect(element.message).to.have.been.calledWith(sinon.match.has('type', 'focus'));
        });

        it('component should receive blur message when trigger is blurred', function () {
            focus(trigger);
            blur(trigger);
            expect(element.message.getCalls().pop().args[0].type).to.equal('blur');
        });

        it('is not sending blur message if focusing to element in controlled component', function () {
            const nestedInput = $('<input id="nested-input">');
            element.appendChild(nestedInput[0]);
            element.style.display = 'block';
            focus(trigger);
            focus(nestedInput);
            expect(element.message).to.have.not.been.calledWith(sinon.match.has('type', 'blur'));
        });

        it('should not toggle when disabled trigger is clicked', function () {
            click(disabledTrigger);
            expect(element.message).to.have.not.been.called;
        });

        it('should not toggle when disabled trigger is hovered', function () {
            hover(disabledTrigger);
            expect(element.message).to.have.not.been.called;
        });

        it('should not toggle when disabled trigger is focused', function () {
            focus(disabledTrigger);
            expect(element.message).to.have.not.been.called;
        });
    });

    describe('Elements -', function () {
        beforeEach(function () {
            element = createElement();
            element.message = sinon.spy();
        });

        it('if a trigger is an anchor, its hyperlink should not be followed', function (done) {
            const spy = sinon.spy();
            trigger = createAnchorTrigger({href: '#trigger-followed'});
            $(window).one('hashchange.aui-test-suite', spy);

            click(trigger);
            afterMutations(() => {
                expect(element.message.callCount).to.equal(1);
                expect(spy.callCount, 'link should not have been followed').to.equal(0);
                done();
            });
        });

        it('if a nested a[href] is clicked, its hyperlink should be followed', function (done) {
            trigger = createAnchorTrigger(
                {href: '#anchor-trigger'},
                '<a href="#nested-anchor"><span id="nested-span">Nested</span></a>'
            );

            expect(element.message).to.not.have.been.called;
            document.getElementById('nested-span').click();
            afterMutations(() => {
                expect(window.location.hash).to.equal('#nested-anchor');
                done();
            });
        });
    });
});
