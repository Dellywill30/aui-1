AJS.$(function() {
    var inlineDialog2El = new AJS.InlineDialog2();
    inlineDialog2El.setAttribute('id', 'nav-item-actions-popup');
    inlineDialog2El.setAttribute('alignment', 'bottom left');
    inlineDialog2El.setAttribute('responds-to', 'toggle');
    inlineDialog2El.querySelector('.aui-inline-dialog-contents').innerHTML = '<p>This is reused</p>';
    document.body.appendChild(inlineDialog2El);

    AJS.$('.aui-nav-item-actions').each(function(i, el) {
        AJS.$(el).attr({
            'data-aui-trigger': '',
            'aria-controls': inlineDialog2El.id
        });
    });

    AJS.$('.aui-sidebar').on('mouseleave', function() {
        AJS.$(this).find('.aui-expander-trigger').trigger('aui-expander-collapse');
    });

    var sidebar = AJS.sidebar('.aui-sidebar');
    var hasUserCollapsedSidebar;
    // prevent auto-expansion if user collapsed the sidebar before
    sidebar.on('expand-start', function (e) {
        if (e.isResponsive && hasUserCollapsedSidebar) {
            e.preventDefault();
        }
    });

    sidebar.on('expand-end collapse-end', function (e) {
        // remember the collapsed state only when it collapses on users request
        // and viewport is not narrow at the moment
        if (!e.isResponsive && !sidebar.isViewportNarrow()) {
            hasUserCollapsedSidebar = sidebar.isCollapsed();
        }
    });

});
