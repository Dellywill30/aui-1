package com.atlassian.test.servlets;


import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.util.Assertions;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.atlassian.test.Config.PLUGIN_KEY;

public class StaticFileServlet extends HttpServlet {
    private final Plugin plugin;
    private static final Logger log = LoggerFactory.getLogger(StaticFileServlet.class);

    public StaticFileServlet(PluginAccessor pluginAccessor) {
        this.plugin = pluginAccessor.getPlugin(PLUGIN_KEY);
        Assertions.notNull("plugin", plugin);
    }

    private String httpPathToResourcePath(String httpPath) {
        return httpPath.replaceFirst("/testPages/", "").replaceFirst("/test-pages/", "");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String pathInfo = req.getPathInfo();
        final String path = httpPathToResourcePath(pathInfo);

        log.info("Loading '{}', taken from '{}'", path, pathInfo);

        String type = Files.probeContentType(Paths.get(path));
        if (null == type || "".equals(type)) {
            if (path.endsWith(".html")) {
                type = "text/html";
            } else if (path.endsWith(".js")) {
                type = "text/javascript";
            } else if (path.endsWith(".css")) {
                type = "text/css";
            } else if (path.endsWith(".png")) {
                type = "image/png";
            } else if (path.endsWith(".jpg")) {
                type = "image/jpg";
            } else if (path.endsWith(".svg")) {
                type = "image/svg+xml";
            }
        }

        resp.setContentType(type);

        InputStream in = plugin.getResourceAsStream(path);
        OutputStream out = resp.getOutputStream();
        IOUtils.copy(in, out);
        out.close();
    }
}
