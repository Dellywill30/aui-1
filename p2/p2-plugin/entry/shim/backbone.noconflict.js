/* eslint-env amd */
define(['actual/backbone'], function(Backbone) {
    return Backbone.noConflict();
});
