/* AUI-4199: This puts the FFI object on the jQuery global for backwards compatibility. */
import FancyFileInput from 'fancy-file-input/dist/fancy-file-input';

export default FancyFileInput;
