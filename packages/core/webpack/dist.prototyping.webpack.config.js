/* eslint-env node */
const { BannerPlugin } = require('webpack');
const merge = require('webpack-merge');
const { librarySkeleton, libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const parts = require('../../../build/webpack/webpack.parts');
const { sourceDir, outputDir } = require('./dist.config');
const { name: packageName } = require('../package.json');

const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');

const bannerPlugin = new BannerPlugin({
    banner: require('./util/banner'),
    raw: true,
    entryOnly: true
});

const mainLibraryConfig = merge([
    librarySkeleton,

    {
        context: sourceDir,
        output: {
            globalObject: 'window',
            libraryTarget: 'umd',
            library: {
                type: 'window',
                name: {
                    amd: packageName,
                    commonjs: packageName,
                    root: 'AJS',
                },
                umdNamedDefine: true,
            },
            path: outputDir,
            publicPath: '',
            uniqueName: '__auiPrototyping',
        },

        resolve: {
            alias: {
                underscore$: require.resolve('underscore'),
                backbone$: require.resolve('backbone')
            }
        },

        optimization: {
            splitChunks: {
                chunks: 'initial',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                minSize: Infinity,
                cacheGroups: {
                    vendors: false
                }
            }
        },

        plugins: [
            bannerPlugin
        ]
    }
]);

const libraryWithInternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping': './aui.batch.prototyping.js',
        },
        externals: [libraryExternals.jqueryExternal],
    }
]);

const libraryWithExternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping.nodeps': './aui.batch.prototyping.js',
        },
        externals: Object.values(libraryExternals),
    }
]);

const deprecationsConfig = merge([
    librarySkeleton,
    {
        entry: {
            'aui-css-deprecations': require.resolve('@atlassian/aui/src/js/aui-css-deprecations.js'),
        },
        output: {
            path: outputDir,
        },
        externals: [libraryExternals.jqueryExternal],
    }
]);
const darkMode = merge([
    parts.extractCss(),
    {
        mode: 'production',
        context: sourceDir,
        output: {
            path: outputDir,
        },
        entry: {
            'aui-prototyping-darkmode': './styles/aui.page.dark-mode.js',
        },
        plugins: [
            //we do not want to output any js
            new IgnoreEmitPlugin(/\.js/),
        ]
    }
]);

module.exports = [
    libraryWithInternalisedDeps,
    libraryWithExternalisedDeps,
    deprecationsConfig,
    darkMode
];
