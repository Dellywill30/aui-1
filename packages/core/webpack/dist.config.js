const path = require('path');
const outputDir = path.resolve(__dirname, '../dist/aui');
const sourceDir = path.dirname(require.resolve('@atlassian/aui/entry/aui.core'));

module.exports = {
    sourceDir,
    outputDir
};
