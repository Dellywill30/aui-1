/**
 * @fileOverview This script checks if dist and bundlesize config are in sync.
 *
 * It will throw an error if there is a file in dist directory not mentioned
 * in config or there is a config entry for which there is no file in dist.
 *
 * It ignores sourcemap files in dist (.map).
 */

var glob = require('glob');
var path = require('path');

var filesWithSizes = require('./dependencies/files-with-sizes');

glob(path.resolve(__dirname, '../dist/**/*.*'), function(er, files) {
    if (er) {
        throw er;
    }

    const fileIdsFromDirectory = files
        .map(filename => {
            const begin = 'packages/core/dist/aui/';
            const indexOfBegin = filename.indexOf(begin);
            const indexOfPath = indexOfBegin + begin.length;
            const rest = filename.slice(indexOfPath);
            return rest;
        })
        .filter(fileId => !fileId.endsWith('.map'));

    const fileIdsFromBundlesizeConfig = Object.keys(filesWithSizes);

    const filesMissingInConfig = fileIdsFromDirectory.filter(
        fileId => !fileIdsFromBundlesizeConfig.includes(fileId)
    );

    if (filesMissingInConfig.length > 0) {
        throw new Error(
            [
                'There are files in the dist directory missing from files-with-sizes.js:',
                ...filesMissingInConfig
            ].join('\n')
        );
    }

    const filesMissingInDirectory = fileIdsFromBundlesizeConfig.filter(
        fileId => !fileIdsFromDirectory.includes(fileId)
    );

    if (filesMissingInDirectory.length > 0) {
        throw new Error(
            [
                'There are filepaths in files-with-sizes.js missing from the dist directory:',
                ...filesMissingInDirectory
            ].join('\n')
        );
    }

    console.log(
        'Bundlesize config verified - it is in sync with the dist directory'
    );
});
