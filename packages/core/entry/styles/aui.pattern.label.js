// todo: this component needs re-naming to avoid confusion with the <aui-label> component.
import './aui.page.reset';
import './aui.page.typography';
import './aui.page.links';
import '@atlassian/aui/src/less/aui-experimental-labels.less';
