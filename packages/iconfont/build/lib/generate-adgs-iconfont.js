const fs = require('fs');
const glob = require('glob').sync;
const mkdirp = require('mkdirp');
const path = require('path');
const webfontsGenerator = require('webfonts-generator');

const glyphsMappings = require('./generate-adgs-iconfont/glyphs-mappings');

const rootPath = path.resolve(__dirname, '..', '..');
const iconsSrc = path.resolve(rootPath, 'src/16px');
const dest = path.resolve(rootPath, 'dist/fonts');
const documentationIconsListPath = path.resolve(rootPath, '../docs/src/assets/icons-list.json'); // todo: common dep between docs and here

const fontName = 'ADGS Icons';
const fontFileName = 'adgs-icons';
const fontId = 'adgs-icons';

const startCodepoint = 0xf1f3;

const templateOptions = {
    baseClassSelector: 'aui-iconfont',
    classPrefix: 'aui-iconfont-',
    baseSelector: '.aui-iconfont'
};

const htmlTemplate = path.resolve(__dirname, 'generate-adgs-iconfont/html.hbs');
const cssTemplate = path.resolve(__dirname, 'generate-adgs-iconfont/css.hbs');

// Work around bug in webfonts-generator by ensuring the output directory exists.
if (!fs.existsSync(dest)) {
    mkdirp.sync(dest);
}

const files = glob('**/*.svg', {
    cwd: iconsSrc
}).map(iconPath => path.resolve(iconsSrc, iconPath));

const codepoints = Object
    .keys(glyphsMappings)
    .map((unicode) => {
        const iconName = glyphsMappings[unicode];

        return {
            iconName,
            unicode: parseInt(unicode, 16)
        };
    }).reduce((result, { iconName, unicode }) => {
        if (result[iconName]) {
            result[iconName].push(unicode);
        } else {
            result[iconName] = [unicode];
        }

        return result;
    }, {});

const sanitizeName = name => name.toLowerCase().replace(/\s+/g, '-');

const rename = (iconPath) => {
    let name = path.basename(iconPath, path.extname(iconPath));
    return sanitizeName(name);
};

// "Special" icon names that differ between the canonical design source and generated code,
// mainly because their names conflict with concepts from ADG 2 that don't neatly map to this new one.
const newNameMap = ['arrow-down', 'arrow-up', 'edit', 'help', 'star', 'watch'];
const mapToNewName = (iconPath) => {
    let name = rename(iconPath);
    if (newNameMap.includes(name)) {
        name = `new-${name}`;
    }
    return name;
};

const options = {
    files, dest, fontName, fontId, templateOptions, codepoints, startCodepoint,
    rename,
    htmlTemplate,
    cssTemplate,
    css: true,
    html: true,
    normalize: true,
    fontHeight: 512,
    writeFiles: false
};

const saveFontResult = (format, content) => {
    const filePath = path.resolve(dest, `${fontFileName}.${format}`);

    fs.writeFileSync(filePath, content);

    return filePath;
};

const formats = ['svg', 'ttf', 'eot', 'woff'];

// Generate icons
webfontsGenerator(options, (error, result) => {
    if (error) {
        console.log('Fail!', error);
        process.exit(1);

        return;
    }

    formats.forEach(format => {
        const content = result[format];
        const filePath = saveFontResult(format, content);
        console.log(`Font ${format} saved to: "${filePath}"`)
    });

    const cssCode = result.generateCss();
    fs.writeFileSync(path.join(dest, 'icons.css'), cssCode);

    // Save icon list for documentation purpose
    fs.writeFileSync(documentationIconsListPath, JSON.stringify(files.map(mapToNewName), null, '  ') + '\n');

    console.log('Done!');
});
