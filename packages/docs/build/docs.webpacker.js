const webpack = require('webpack');
const webpackConfig = require('./docs.webpack.config');

module.exports = (opts) => function buildFrontend(done) {
    const finalConfig = Object.assign(webpackConfig, opts);
    webpack(finalConfig, function (err, stats) {
        let errors = [].concat(err);
        if (stats) {
            const json = stats.toJson();
            errors = errors.concat(json.errors);
        }
        errors = errors.filter(Boolean);

        if (errors.length) {
            console.error('webpack compilation produced errors', errors);
            done(errors);
        } else {
            // if (stats.hasWarnings()) {
            //     console.warn('webpack compilation produced warnings', info.warnings);
            // }
            done();
        }
    });
};
