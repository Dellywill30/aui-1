---
component: AUI 9 upgrade guides
pageCategory: upgrade
analytics:
  pageCategory: upgrade-guide
  component: all
---

<aui-docs-contents></aui-docs-contents>

<style>
    .wrm-key {
        font-size: smaller;
    }

    code {
        background-color: #f4f5f7; {{!-- N20 --}}
        border-radius: 3.01px;
        padding: 0.2em 0.25em;
    }
</style>

<h2 id="8-to-9">Upgrading from 8.x to 9.0</h2>
<h3 id="9.0-a11y">Accessibility changes</h3>
<h4>Dialog2</h4>
<h5>Focus management</h5>
<p>
    The dialog element itself is now focusable by JavaScript.
    It will be focused as the first thing after dialog being opened unless
    you set explicitly the <code>data-aui-focus-selector</code> attribute.
</p>
<h5>Close button</h5>
<p>Make sure the close button:</p>
<ul>
    <li>is a button (and has a <code>type="button"</code> just in case you put it within HTML form somehow)</li>
    <li>has an aria-label attribute, with a translation string that evaluates to "Close"</li>
    <li>is in tab order (it will be by default if it's a button)</li>
</ul>
<p>Markup example:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button type="button" class="aui-close-button" aria-label="{getText('aui.words.close')}"></button>
    </noscript>
</aui-docs-example>

<h4 id="9.0-inlinedialo2">Inline Dialog 2</h4>
<p>Inline dialog element is now focused after opening.</p>
<p>Make sure you set <code>aria-label</code> on <code>aui-inline-dialog</code> element to give screen reader friendly
    description of the the dialog's content.
    When aria-label is missing the AT will read whole content of the dialog.</p>

<h4 id="9.0-layered-a11y">Layered elements</h4>
<div class="aui-message aui-message-info">Layered elements except Dialog2 are no longer appended to the body.</div>
<p>
    Layered elements except Dialog2 will not alter their allocation in DOM tree by default. <br />
    To maintain a natural tab order for keyboard users, layered elements should be rendered as the next sibling of their trigger.
</p>
<p>
    It may happen that due to the design of your application the layered components may get obscured - i.e. being clipped
    by the parent container with <code>overflow:hidden;</code> CSS property set.
</p>
<p>You may need to alter your application's CSS to adjust to the new behaviour.</p>
<p>
    In rare cases when such adjustments are not possible - you may specify a rendering DOM container for any layered component.<br />
    Upon displaying of such layered component - it will temporarily move it's subtree into the specified container - allowing for unobstructed rendering.<br />
    You should always choose the closest container allowing for unobstructed render.<br />
    You are responsible for ensuring the component is properly cleaned up, adequately to your application's lifecycle.
</p>
<div class="aui-message aui-message-warning">
    <p class="title">
        <strong>A11y warning</strong>
    </p>
    <p>Specifying designated rendering DOM container may impact tab flow, information flow, focus or keyboard navigation patterns of the component.</p>
    <p>It may result in your application being inaccessible for users dependant on keyboard navigation, controller navigation and/or using NVDA, VoiceOver or other assistive technologies.</p>
</div>
<p>
    In order to specify a rendering DOM container, set the <code class="first-use">data-aui-dom-container</code> attribute
    to a selector matching the container element:
</p>
<aui-docs-example live-demo>
<noscript is="aui-docs-code" type="text/html">
<div id="dom-container">
    <aside>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consectetur eius enim laboriosam nulla quaerat, qui veniam. Adipisci amet architecto, deleniti dicta dolore esse in libero, mollitia perferendis placeat tempora!</aside>
    <section>
        <button class="aui-button aui-dropdown2-trigger" aria-controls="cropped">
            cropped
        </button>
        <aui-dropdown-menu id="cropped" data-aui-dom-container="#dom-container">
            <aui-section>
                <aui-item-link href="http://amazon.com">Amazon</aui-item-link>
                <aui-item-link href="http://apple.com">Apple</aui-item-link>
            </aui-section>
        </aui-dropdown-menu>
    </section>
</div>
</noscript>
<noscript type="text/css">
#dom-container {
    border: 1px dashed #999;
    padding: 10px;
    width: 500px;
    display: flex;
    flex-direction: row-reverse;
}

#dom-container section {
    position: relative;
    overflow: hidden;
    width: 100px;
    flex: none;
}
</noscript>
</aui-docs-example>
<ul>
    <li>If <code>data-aui-dom-container</code> attribute is set but the selector matches multiple elements - it will default to first available match.</li>
    <li>If <code>data-aui-dom-container</code> attribute is set but the selector does not match to any existing elements it will default to <code>document.body</code></li>
    <li>If <code>data-aui-dom-container</code> attribute is not set, the dropdown will be left in place.</li>
</ul>
<p>
    Even though this API is available for all layered components, only the components that guarantee adequate tab, keyboard and/or focus flow management will expose this API in their documentation.<br />
    Always refer to the component's documentation to see if it exposes any means to overcome similar rendering obstacles, i.e. by setting proper alignment.
</p>

<h4 id="9.0-dropdown2-a11y">Dropdown2</h4>
<h5>Styling changes</h5>
<p>
    Previously, specific ARIA attributes would receive styling when used in a dropdown container.
    Now, ARIA attributes receive no explicit styling.
</p>
<p>
    If you are using the markup-based pattern (not the web-component pattern) for your dropdowns,
    You must use one the following patterns to receive the dropdown item aesthetic:
</p>
<ul>
    <li>Use an <var>&lt;a&gt;</var> HTML element if the dropdown item will take the user to another page.</li>
    <li>Use a <var>&lt;button&gt;</var> HTML element if the dropdown item will do something dynamic.</li>
    <li>Use the <var>.aui-dropdown2-radio</var> or <var>.aui-dropdown2-checkbox</var> CSS classes to make an item present as a radio or checkbox respectively.</li>
</ul>
<p>
    Note that these are changes to the CSS and affect visual aesthetic. You are responsible for using valid, semantic markup
    so that the items are perceived correctly in non-visual contexts, such as screen readers.
</p>

<h4 id="9.0-appheader">Application header</h4>
<h5>Markup changes</h5>

<p>
    Previously, the application header's logo was wrapped in a <var>&lt;h1&gt;</var> element.
    The <var>&lt;h1&gt;</var> element should only be used inside the page's main content.
    The logo element has now been replaced with a  <var>&lt;span&gt;</var> element.
</p>

<p>
    The pattern's <var>&lt;nav&gt;</var> element must have an <var>aria-label</var> with
    a translated value of "site". This allows screen reader users to perceive the application
    header as "site navigation".
</p>

<p>
    Make sure that the logo link has translation string that interpolates to "Go to home page" label.
</p>

<table class="aui">
    <thead>
    <tr>
        <th>Old markup</th>
        <th>New markup</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <nav class="aui-header">
                        <div class="aui-header-primary">
                            <h1 id="logo" class="aui-header-logo ...">
                                <!-- Your site name + logo -->
                            </h1>
                        </div>
                    </nav>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <nav class="aui-header" role="navigation" aria-label="site">
                        <div class="aui-header-primary">
                            <span id="logo" class="aui-header-logo ...">
                                <!-- Keep in mind that labels should be translated -->
                                 <a aria-label="Go to home page">
                                <!-- Your site name + logo -->
                            </span>
                        </div>
                    </nav>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<h4>Page layout changes</h4>
<h5>The <var>.aui-panel</var> class is gone</h5>
<p>
    AUI used to include an <var>.aui-panel</var> CSS class.
    Its purpose was wholly replaced by <var>.aui-page-panel</var> in AUI 5.
    The upgrade is a simple 1:1 mapping: find and replace all <var>.aui-panel</var> with <var>.aui-page-panel</var>.
</p>

<h5>Page structure changes</h5>
<p>
    The <a href="{{rootPath}}docs/page.html">page layout pattern</a> was updated; some markup changes
    were made to allow easier placement of key landmark roles, as well as to provide recommended labeling
    for each. Of note in the changes are:
</p>

<ul>
    <li>
        Where to place the <var>&lt;main&gt;</var> element on your page.
    </li>
    <li>
        The position of the <a href="{{rootPath}}docs/sidebar.html">sidebar</a> has changed in markup.
    </li>
    <li>
        The recommended tag names of interstitial markup between page structure and content are slightly updated.
    </li>
    <li>
        Removal of redundant <var>role</var> attributes where all supported browsers and screen readers understand
        the native semantic of recommended tags.
    </li>
</ul>

<p>
    Here are how some typical page layouts have changed in markup:
</p>

<table class="aui">
    <caption>Changes to overall page structure</caption>
    <tbody>
    <tr>
        <td>
            <aui-docs-example label="Old page structure markup pattern">
                <noscript is="aui-docs-code" type="text/html">
                    <div id="page">
                        <header id="header" role="banner">
                            <nav>
                                <!-- application header here -->
                            </nav>
                        </header>
                        <section id="content" role="main">
                            <!-- page content patterns here -->
                        </section>
                        <footer id="footer" role="contentinfo">
                            <section class="footer-body">
                                <!-- site info and footer links here -->
                            </section>
                        </footer>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example label="New page structure markup pattern">
                <noscript is="aui-docs-code" type="text/html">
                    <div id="page">
                        <header id="header" role="banner">
                            <nav aria-label="site">
                                <!-- application header here -->
                            </nav>
                        </header>
                        <div id="content">
                            <!-- page content patterns here -->
                        </div>
                        <footer id="footer" role="contentinfo">
                            <div class="footer-body">
                                <!-- site info and footer links here -->
                            </div>
                        </footer>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<table class="aui">
    <caption>Changes to page content patterns</caption>
    <tbody>
    <tr>
        <td>
            <aui-docs-example label="Old page content markup pattern">
                <noscript is="aui-docs-code" type="text/html">
                    <div id="content" role="main">
                        <div class="aui-sidebar">
                            <!-- project- or space-level nav items-->
                        </div>
                        <header class="aui-page-header"><!-- ... --></header>
                        <nav class="aui-navgroup aui-navgroup-horizontal">
                            <!-- page- or section-level nav items -->
                        </nav>
                        <div class="aui-page-panel">
                            <div class="aui-page-panel-inner">
                                <div class="aui-page-panel-nav">
                                    <!-- in-page nav items -->
                                </div>
                                <div class="aui-page-panel-content">
                                    <p>My content begins here</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example label="New page content markup pattern">
                <noscript is="aui-docs-code" type="text/html">
                    <div id="content">
                        <section class="aui-sidebar" aria-label="sidebar">
                            <!-- project- or space-level nav items-->
                        </section>
                        <section aria-label="page">
                            <div class="aui-page-header"><!-- ... --></div>
                            <nav class="aui-navgroup aui-navgroup-horizontal" aria-label="page">
                                <!-- page- or section-level nav items -->
                            </nav>
                            <div class="aui-page-panel">
                                <div class="aui-page-panel-inner">
                                    <nav class="aui-page-panel-nav">
                                        <!-- in-page nav items -->
                                    </nav>
                                    <main class="aui-page-panel-content" id="main" role="main">
                                        <p>My content begins here</p>
                                    </main>
                                </div>
                            </div>
                        </section>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<p>
    Please read the full <a href="{{rootPath}}docs/page.html">page layout documentation</a> page for more details
    on these changes.
</p>

<h5>Soy template changes</h5>
<p>
    Previously, the <var>aui.page.pagePanelContent</var> template output a <var>&lt;section&gt;</var> element
    and had no <var>id</var> value.
</p>
<p>
    In AUI 9, this template now outputs a <var>&lt;main&gt;</var> element with a default <var>id</var> value of
    <var>main</var>. It will also output the appropriate <var>role</var> attribute.
</p>

<table class="aui">
    <caption>The following table demonstrates the differences in call output.</caption>
    <tbody>
    <tr>
        <td>
            <aui-docs-example label="Old call with no params">
                <noscript is="aui-docs-code" type="text/soy">
                    {call aui.page.pagePanelContent}
                        {param content}<p>Hello, world!</p>{/param}
                    {/call}
                </noscript>
                <noscript is="aui-docs-code" type="text/html">
                    <section class="aui-page-panel-content">
                        <p>Hello, world!</p>
                    </section>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example label="New call with no params">
                <noscript is="aui-docs-code" type="text/soy">
                    {call aui.page.pagePanelContent}
                        {param content}<p>Hello, world!</p>{/param}
                    {/call}
                </noscript>
                <noscript is="aui-docs-code" type="text/html">
                    <main role="main" id="main" class="aui-page-panel-content">
                        <p>Hello, world!</p>
                    </main>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    <tr>
        <td>
            <aui-docs-example label="Old call with explicit params">
                <noscript is="aui-docs-code" type="text/soy">
                    {call aui.page.pagePanelContent}
                        {param tagName: 'div' /}
                        {param content}<p>Hello, world!</p>{/param}
                    {/call}
                </noscript>
                <noscript is="aui-docs-code" type="text/html">
                    <div class="aui-page-panel-content">
                        <p>Hello, world!</p>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example label="New call with explicit params">
                <noscript is="aui-docs-code" type="text/soy">
                    {call aui.page.pagePanelContent}
                    {param tagName: 'div' /}
                    {param content}<p>Hello, world!</p>{/param}
                    {/call}
                </noscript>
                <noscript is="aui-docs-code" type="text/html">
                    <div id="main" class="aui-page-panel-content">
                        <p>Hello, world!</p>
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<h3 id="9.0-aria-hidden">Changes to hiding and showing elements</h3>
<p>AUI used to use <code>aria-hidden</code> attribute to visually hide elements, which is an incorrect usage of this attribute.</p>
<p>To improve how elements are shown and hidden, we made the following changes:</p>
<ul>
    <li>We removed invalid uses of <code>aria-hidden</code> and replaced some of them with <code>hidden</code>.</li>
    <li>For several elements, we now use the <code>open</code> attribute to indicate whether the element should be visible or not.</li>
    <li>AUI's CSS reset sets <code>display:none !important</code> on any element with the <var>hidden</var> attribute.</li>
</ul>

<p>
    If your code tests for the visibility of an element, check for the presence of the <code>open</code>
    attribute and the absence of a <code>hidden</code> attribute.
</p>

<p>
    If an element should be hidden immediately, add the <code>hidden</code> attribute to it.
</p>

<p>
    If you animate any elements when they appear or disappear, you should do this with the <code>open</code>
    attribute. The <code>hidden</code> attribute takes precedence over <code>open</code> and will take effect
    immediately; it will not be possible to animate visibility when this attribute is added to an element.
</p>

<h4 id="9.0-dialog2">Dialog2</h4>
<p>Previously in our docs examples markup for Dialog2 included <code> aria-hidden="true"</code> attribute on <code>section</code> element.</p>
<p>Now you should add <code>hidden</code> attribute.</p>
<table class="aui">
    <thead>
    <tr>
        <th>Old markup</th>
        <th>New markup</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <section ... aria-hidden="true">
                        <!-- dialog content !-->
                    </section>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <section ... hidden>
                        <!-- dialog content !-->
                    </section>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>
<h4 id="9.0-dropdown">Dropdown2</h4>
<p>If you are still constructing dropdowns using markup instead of webcomponent you may need to update your HTML.
If you are using Soy templates provided by AUI you are fine if not, make changes described below.</p>
<table class="aui">
    <thead>
    <tr>
        <th>Old markup</th>
        <th>New markup</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <div class="aui-dropdown2"
                         ...
                         aria-hidden="true">
                        <!-- dropdown content !-->
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <div class="aui-dropdown2"
                         ...
                         hidden>
                        <!-- dropdown content !-->
                    </div>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<h4 id="9.0-layer">Layer component</h4>
<p>Refactoring aria-hidden attributes required also changes in Layer component.</p>
<table class="aui">
    <thead>
    <tr>
        <th>Old behaviour</th>
        <th>New behaviour</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            If <code>.hide()</code> was called on a layer element:
            <ul>
                <li>that did not have <code>aria-hidden</code> attribute -> <code>add aria-hidden="false"</code> attribute</li>
                <li>that did have <code>aria-hidden="false"</code> attribute -> do nothing</li>
                <li>that did have <code>aria-hidden="true"</code> attribute -> hide element</li>
            </ul>

        </td>
        <td>
            If <code>.hide()</code> was called on a layer element:
            <ul>
                <li>that did not have <code>hidden</code> attribute -> hide element</li>
                <li>that did have <code>hidden</code> attribute -> do nothing</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
<h3 id="8.x-9.0-lib">AUI library changes</h3>
<h4>Markup changes</h4>
<h5>Sidebar expand button</h5>
<p>We replaced custom icon with icon font. Make sure that the span now has <code>aui-iconfont-chevron-double-left</code> class.</p>
<table class="aui">
    <thead>
    <tr>
        <th>Old markup</th>
        <th>New markup</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <a class="aui-button ..." title="Collapse sidebar ( [ )">
                        <span class="aui-icon aui-icon-small"></span>
                    </a>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <button class="aui-button ..." title="Collapse sidebar ( [ )">
                        <span class="aui-icon aui-icon-small aui-iconfont-chevron-double-left"></span>
                    </button>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>
<h4>CSS changes</h4>
<h5>"Message" close icon was removed. Following classes are no longer supported:</h5>
<ul>
    <li>.aui-message .icon-close</li>
    <li> .aui-icon-close</li>
    <li>.aui-message .icon-close-inverted</li>
    <li>.aui-message.error .icon-close</li>
    <li>.aui-icon-close-inverted</li>
</ul>

<h4>jQuery configuration</h4>
<p>jQuery ajaxSettings configuration was removed. Please specify it globaly for product or add it per request where needed. More information here <a href="https://api.jquery.com/jQuery.param/">https://api.jquery.com/jQuery.param/</a></p>

<h4>Datepicker placeholders</h4>
<p>In previous versions AUI relied on the <code>dateFormat</code> option from the jQuery UI datepicker widget. It was used as a placeholder for the input of type "date" if the "overrideBrowserDefault" option was set to "true".</p>

<p>Since the default format is represented by string "yy-mm-dd" it was confusing users because "yy" stands for the <strong>long year format (4-digits)</strong> - not <strong>the short, 2-digits year format</strong>.</p>

<p>We decided to break the dependency between the input placeholder and the format string by extending the configuration API and allowing the placeholder to be set explicitly. That requires you to review all the usages of the datepicker widget and set placeholders where they are needed to clarify understanding of given input purpose.</p>

<p>Our recommendation here is to not use placeholders and put the expected date format next to the input label - mostly for the accessibility reasons. <strong>Use placeholders only as a last resport.</strong> We suggest representing real values - not the format string: <code>eg. 2020-01-29</code> instead of <code>yyyy-mm-dd</code></p>

<h2 id="9-to-9.1">Upgrading from 9.0.x to 9.1</h2>
<h3 id="9.0.x-9.1-lib">AUI library changes</h3>
<p>AUI's version of <code>Popper.js</code> - a third party library supporting AUI's layered components - has been bumped to v2.4.4, up from v1.14.5.</p>
<h4>Markup changes</h4>
<p>
    Due to changes introduced with <code>Popper.js</code> - shape of certain attributes might have changed for those components.<br />
    <code>x-*</code> have been replaced with <code>data-popper-*</code><br />
    For details - visit <a href="https://popper.js.org/docs/v2/migration-guide/#3-change-your-css-attribute-selectors"> Popper.js Migration guide</a>
</p>
<table class="aui">
    <thead>
    <tr>
        <th>Old markup</th>
        <th>New markup</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <aui-inline-dialog x-placement="top" ... >
                    </aui-inline-dialog>
                </noscript>
            </aui-docs-example>
        </td>
        <td>
            <aui-docs-example>
                <noscript is="aui-docs-code" type="text/html">
                    <aui-inline-dialog data-popper-placement="top" ... >
                    </aui-inline-dialog>
                </noscript>
            </aui-docs-example>
        </td>
    </tr>
    </tbody>
</table>

<p>
    Even though these attributes have been used as internal API only and not explicitly guaranteed - for compatibility reasons - <code>x-placement</code> attribute is provided by AUI layered components.<br />
    Its use is considered as DEPRECATED, meaning - the attribute will be removed with further versions of AUI. If your application depends on the provided data - please adapt to new, namespaced markup <code>data-popper-*</code>
</p>

<h2 id="9.2-to-9.3">Upgrading from 9.2.x to 9.3.0</h2>

<h3 id="9.2-to-9.3-lib">Library changes</h3>
<p>
    To reduce the bundle size and remove redundant layer management solution the jQuery.tipsy was switched 
    to Popper.js which is already the layer management library of choice for AUI.
</p> 
<p>
    That means that any code that was calling jQuery.tipsy API directly, using options undocumented in AUI API 
    may no longer work as expected.
</p>
<p>
    Some behaviours have been hard-coded to match the ADG recommendations, eg. 
    delay for tooltip appearance, 
    removal of animation, 
    removal of arrows pointing from tooltip to its trigger.
</p>

<h3 id="9.2-to-9.3-architecture">Architectural changes</h3>
<p>
    The new implementation of tooltips relies on single DOM node inserted at the bottom of document. 
    Its text content changes every time new tooltip shows up. 
    This was done mostly because removal of tooltip animations makes it impossible to have more than one tooltip visible
    in the same time on screen. It also slightly impreves the DOM mutation performence.
</p>