const React = require("react");

module.exports = () => (
    <footer id="footer" role="contentinfo">
        <section class="footer-body">
            <ul>
                <li>I &hearts; AUI</li>
            </ul>

            <div id="footer-logo">
                <a href="https://www.atlassian.com/" target="_blank">
                    Atlassian
                </a>
            </div>
        </section>
    </footer>
);
