const React = require('react');

const Banner = require('../components/Banner');

module.exports = ({ lang }) => (
    <>
        {lang === 'en' && (
            <Banner ariaLabel="site announcements">
                You are browsing the <strong>accessibility demo app</strong>{' '}
                built with AUI. For technical details go to{' '}
                <a href="/docs">the official documentation</a>.
            </Banner>
        )}

        {lang === 'pl' && (
            <Banner ariaLabel="site announcements">
                To jest <strong>aplikacja demonstracyjna</strong> stworzona przy
                pomocy AUI. Po więcej szczegółów technicznych otwórz{' '}
                <a href="/docs">oficjalną dokumentację (po angielsku)</a>.
            </Banner>
        )}
    </>
);
