const React = require("react");
const classnames = require("classnames");

const MetalsmithContext = require("../../../build/MetalsmithContext");

module.exports = ({ title, mode, size, scripts, children }) => {
    const { auiVersion, lang, homePage } = React.useContext(MetalsmithContext);

    const urls = {
        jquery: "https://unpkg.com/jquery@3.4.1/dist/jquery.js",
        auiPrototypingJS: `https://unpkg.com/@atlassian/aui@${auiVersion}/dist/aui/aui-prototyping.js`,
        auiPrototypingCSS: `https://unpkg.com/@atlassian/aui@${auiVersion}/dist/aui/aui-prototyping.css`,
        auiPrototypingDarkModeCSS: `https://unpkg.com/@atlassian/aui@${auiVersion}/dist/aui/aui-prototyping-darkmode.css`,
    };

    const bodyClassName = classnames({
        "aui-page-focused": mode === "focused",
        "aui-page-size-small": size === "small",
        "aui-page-size-medium": size === "medium"
    });

    return (
        <html lang={lang}>
            <head>
                <meta charset="utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <title>{title}</title>

                <link rel="stylesheet" href={urls.auiPrototypingCSS} />
                <link rel="stylesheet" href={urls.auiPrototypingDarkModeCSS} />
                <style>{`.aui-header aui-toggle .aui-icon { --aui-icon-size: 16px; }`}</style>
            </head>

            <body className={bodyClassName} data-home-page={homePage}>
                {children}
                <script src={urls.jquery}></script>
                <script src={urls.auiPrototypingJS}></script>

                {(scripts || []).map(script => (
                    <script key={script} src={script}></script>
                ))}
            </body>
        </html>
    );
};
