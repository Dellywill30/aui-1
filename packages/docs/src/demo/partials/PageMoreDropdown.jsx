const React = require('react');

const Button = require('../components/Button');

const MetalsmithContext = require('../../../build/MetalsmithContext');

module.exports = () => {
    const { translate } = React.useContext(MetalsmithContext);

    return (
        <>
            <Button
                variant="subtle"
                controlledDropdownId="compact-button-dropdown"
            >
                <span class="aui-icon aui-icon-small aui-iconfont-more">
                    {translate('More')}
                </span>
            </Button>

            <aui-dropdown-menu id="compact-button-dropdown">
                <aui-item-link href="#">Menu item 1</aui-item-link>
                <aui-item-link href="#">Menu item 2</aui-item-link>
                <aui-item-link href="#">Menu item 3</aui-item-link>
            </aui-dropdown-menu>
        </>
    );
};
