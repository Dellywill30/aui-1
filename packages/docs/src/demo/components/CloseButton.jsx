const React = require('react');

module.exports = () => {
    return <button class="aui-close-button" type="button" aria-label="close"></button>;
};
