const React = require('react');

module.exports = () => {
    return (
        <div class="aui-avatar aui-avatar-small">
            <div class="aui-avatar-inner">
                <img src="https://api.adorable.io/avatars/200/abott4@adorable.png" alt="John Doe" />
            </div>
        </div>
    );
};
