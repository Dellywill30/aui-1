/* eslint-disable */

const labelToId = require('../helpers/labelToId');
const textToTree = require('../helpers/textToTree');
const marked = require('marked');

const hierarchy = `
Wsparcie techniczne
    Baza wiedzy
        Rozwiązywanie problemów
            Zepsuta klawiatura
            Problemy dostępu do VPN
            Rozwiązywanie problemów z połączniem WIFI
            Rozwiązywanie problemów z drukowaniem PDF 
        Artykuły pomocowe
            Instalowanie Microsoft Excel
            Konfiguracja Macbook Pro dla nowych użytkowników
            Mac OS X: Jak dodać drukarkę w sieci
            Konfiguracja klienta pocztowego na Macu
            Prośba o nowego MacBook Pro - proces i specyfikacja
            Zwiększenie powierzchni dyskowej
            Aktualizacja klienta VPN na Mac
            Konfiguracja maila i kalendarza w Windows 10
            Konfiguracja MS Office 365 dla biznesu
            Wizyta w biurze - co powinieneś wiedzieć i zrobić przed
            Konfiguracja nowego laptopa Lenovo Thinkpad
            Jak skonfigurować Google Calendar
            Jak podłączyć Maca do Sieci WiFi
            Jak podłączyć się do drukarki w biurze
            Jak podłączyć PC do sieci WiFi
            Polityki mobilne dla użytkowników
    Katalog dostępnych usług
        Email i narzędzia do pracy wspólnej
        Hosting
        Videokonferencje
    Reakcja na poważne incydenty - przewodniki rozwiązywania problemów
    Inżynieria niezawodności witryny
Kosmos - operacje
    Notatki ze spotkań
        2020-02-20 Notatka ze spotkania
        Szablon notatki ze spotkania
        Kosmos Konferencja 2020
    Planowanie produktu
        Kosmos 2.0 - wiadomość od założycieli
        Kosmos raport finansowy
        Kosmos cennik
        Kosmos mapa drogowa funkcjonalności
    Wymagania produktowe
        Wersja 1.0
		Aplikacja mobilna
    Kosmos PR
        Pierścienie Saturna - ogłoszenie na blogu
        Kosmos - rozpoczęcie akcji promocyjnej
Przestrzeń osobista
    Rozwój osobisty
        Ocena pracownicza
        Cele 2020
        Cele 2019
        Cele 2018
    Wystąpienia i prezentacje
        Przestrzeń kosmiczna Konferencja 2020
        Zgłoszenia na konferencje
Witamy
`;

const spaceItSupportOverview = `
## Witamy w dziale wsparcia technicznego firmy Kosmos. Jak możemy pomóc?

### Co należy do naszych obowiązków?

- Konfigurowanie nowego sprzętu i oprogramowania
- Dbanie o bezpieczeństwo cyfrowe firmy
- Utrzymanie i zarządzanie chmurą
- Niezbędna konfiguracja systemu
- Kontrola i naprawy laptopów i innych urządzeń osobistych
- Konserwacja i naprawa serwerów

### Jeżeli mam problem...

- Najpierw sprawdź listę stron rozwiązywania problemów w nawigacji.
- Jeśli nie ma artykułu odpowiadającego Twojemu problemowi, skontaktuj się z nami bezpośrednio za pośrednictwem portalu serwisowego.
`;

const welcomeOverview = `
## Nowości

### Nasz nowy podcast - The Final Frontier

Opublikowano dzisiaj

Wczoraj uruchomiliśmy nasz pierwszy podcast - The Final Frontier.
Mamy nadzieję, że będzie to kolejne modne miejsce, w którym będziemy rozmawiać o kosmosie, fizyce i życiu.
To także miejsce, w którym chcemy podzielić się „historią za historią" zespołu Kosmos, który wspólnie zrobił niesamowite rzeczy. Nie zapomnij zasubskrybować podcastów Spotify i Apple.

### Wielka Impreza w Kosmosie - ogłoszenie

Opublikowano 4 dni temu

Czekanie zakończone - nasza wielka coroczna impreza Kosmos jest tuż tuż.
Dołącz do nas {today + 10 days} na drinki, jedzenie i taniec w restauracji Podniebie. Ten rok jest dla nasz przecież wyjątkowy! 
Mamy wiele do świętowania. To nasz 20 rok, a my zamierzamy wprowadzić nowy rewolucyjny produkt!
`;

const brokenKeyboardContent = `
Rozumiemy Cię, nic nie jest bardziej frustrujące niż zepsute urządzenie gdy próbujesz wykonać swoją pracę.
Oto lista prostych wskazówek dotyczących rozwiązywania problemów z klawiaturą, która wydaje się uszkodzona.
Wypróbuj je najpierw, zanim poprosisz o nową.

## Przypadek 1 - Wylałem kawę / wodę / pepsi / wino na klawiaturę

Rozlanie drinka na klawiaturze nie musi być pocałunkiem 
śmierci - pod warunkiem że wiesz jak działać szybko.
Weź suchą szmatkę i zetrzyj nadmiar płynu z powierzchni. 
Odwróć ją do góry nogami, połóż na ręczniku lub czymś chłonnym i pozwól wodzie spłynąć.
Pozostaw na 24 godziny. Jeśli nie masz czasu, minimalny czas to cztery godziny. 
Mimo, że klawiatura może wydawać się sucha, części te absorbują dużo wody, dzięki czemu daje tylko czas na rozproszenie jakiegokolwiek płynu.

## Przypadek 2 - Klawisz jest zepsuty

Jeśli jeden z określonych klawiszy jest uszkodzony, sposób jego wymiany będzie zależeć od rodzaju posiadanej klawiatury.
Na przykład klawiatura mechaniczna została zaprojektowana inaczej niż urządzenie z cichym klawiszem.
Jeśli klawisz jest zablokowany, możesz użyć trochę sprężonego powietrza aby usunąć okruchy blokujące jego działanie.

Skontaktuj się z naszym wsparciem, aby nas zbadać, aby zamówić nowy klucz zapasowy lub nową klawiaturę.

Możesz przejść do instrukcje.com, aby uzyskać pomocny film na temat naprawiania niereagującego klawisza na standardowej i często używanej klawiaturze Microsoft, przy użyciu zwykłej plastikowej słomki.

## Przykład 3 - Cała klawiatura nie działa

Przed skontaktowaniem się z naszym działem IT zapoznaj się z tą krótką listą kontrolną.

- Sprawdź baterie. Brzmi prosto, ale to powinno być Twoim pierwszym krokiem. Jeśli masz klawiaturę bezprzewodową, sprawdź nowe baterie i zobacz czy klawiatura działa.
- Sprawdź połączenie. Jeśli masz klawiaturę przewodową, upewnij się, że kabel nie poluzował się z portu USB. Jeśli masz odbiornik USB na klawiaturę bezprzewodową, upewnij się, że jest on prawidłowo podłączony.
- Ponownie sparuj klawiaturę, jeśli korzystasz z technologii Bluetooth. Chociaż większość firm obiecuje jednorazowe sparowanie, czasami konieczne jest wykonanie ponownej czynności. Postępuj zgodnie z instrukcjami krok po kroku dotyczącymi parowania urządzeń Bluetooth.
`;

const workInProgress = `
It's work in progress.
`;

const tree = textToTree(hierarchy);

function replacer(key, value) {
    // Filtering out recursive properties
    if (key === 'parent') {
        return undefined;
    }
    return value;
}

function log(object) {
    console.log(JSON.stringify(object, replacer, 2));
}

// log(tree);

const spaces = tree.map((el) => ({ label: el.label, id: el.id }));

// log(spaces);

const flatTree = (tree, spaceId, spaceLabel) => {
    return tree.map((el) =>
        [{ id: el.id, label: el.label, spaceId, spaceLabel }].concat(
            flatTree(el.children, spaceId, spaceLabel)
        )
    ).reduce((result, next) => result.concat(next), [])
};

const pagesFromTree = tree.map((node) =>
    [
        {
            id: node.id,
            label: node.label,
            spaceId: node.id,
            spaceLabel: node.label,
        },
    ].concat(flatTree(node.children, node.id, node.label))
).reduce((result, next) => result.concat(next), [])

// log(pagesFromTree);

const pagesWithContent = {
    'zepsuta-klawiatura': {
        content: marked(brokenKeyboardContent),
    },
    'wsparcie-techniczne': {
        content: marked(spaceItSupportOverview),
    },
    witamy: {
        content: marked(welcomeOverview),
    },
};

const defaultContent = marked('Work in progress.')

const labelToPage = (label) => ({ id: labelToId(label), label });

const result = {
    tree,
    pages: pagesFromTree.map((page) => {
        const pageWithContent = pagesWithContent[page.id] || {
            content: defaultContent
        };
        return Object.assign({}, page, pageWithContent);
    }),
    menus: {
        spaces,
        recent: {
            recentWork: [
                'Kosmos Konferencja 2020',
                '2020-02-20 notatka ze spotkania',
                'Kosmos - rozpoczęcie akcji promocyjnej',
                'Konfiguracja klienta pocztowego na Macu',
            ].map(labelToPage),
            recentlyVisited: [
                'Zepsuta klawiatura',
                'Email i narzędzia do pracy wspólnej',
                'Szablon notatki ze spotkania',
            ].map(labelToPage),
        },
    },
};

module.exports = result;
