/* eslint-disable */

const dictionary = {
    Notifications: {
        pl: 'Powiadomienia',
    },
    'Skip to main content': {
        pl: 'Przeskocz do kluczowej treści',
    },
    'Log in': {
        pl: 'Zaloguj się',
    },
    'Log In': {
        pl: 'Zaloguj się',
    },
    'App designed for accessibility testing of AUI': {
        pl: 'Aplikacja do celów testów dostępności',
    },
    Questions: {
        pl: 'Pytania',
    },
    Calendars: {
        pl: 'Kalendarze',
    },
    Create: {
        pl: 'Utwórz',
    },
    'Create page': {
        pl: 'Utwórz stronę',
    },
    Space: {
        pl: 'Przestrzeń',
    },
    required: {
        pl: 'wymagane',
    },
    Select: {
        pl: 'Wybierz',
    },
    'Page title': {
        pl: 'Tytuł strony',
    },
    Cancel: {
        pl: 'Anuluj',
    },
    Help: {
        pl: 'Pomoc',
    },
    Pages: {
        pl: 'Strony',
    },
    Settings: {
        pl: 'Ustawienia',
    },
    Spaces: {
        pl: 'Przestrzenie',
    },
    'Space directory': {
        pl: 'Folder przestrzeni',
    },
    'Create space': {
        pl: 'Utwórz przestrzeń',
    },
    Recent: {
        pl: 'Ostatnie',
    },
    'Create new page': {
        pl: 'Utwórz nową stronę',
    },
    'Log out': {
        pl: 'Wyloguj',
    },
    More: {
        pl: 'Więcej',
    },
    unrestricted: {
        pl: 'niezabezpieczone',
    },
    Edit: {
        pl: 'Edytuj',
    },
    'Save for later': {
        pl: 'Zapisz na później',
    },
    Watch: {
        pl: 'Obserwuj',
    },
    Share: {
        pl: 'Udostępnij',
    },
    'Share page': {
        pl: 'Udostępnij stronę',
    },
    'Email address': {
        pl: 'Adres email',
    },
    'Welcome to intranet': {
        pl: 'Witaj w intranecie',
    },
    Username: {
        pl: 'Nazwa użytkownika',
    },
    Password: {
        pl: 'Hasło',
    },
    'Remember my login on this computer': {
        pl: 'Zapamiętaj mnie na tym komputerze',
    },
    'Not a member? To request an account, please contact your administrator.': {
        pl:
            'Nie masz konta? Aby je uzyskać skontaktuj się z Twoim administratorem.',
    },
    [`Can't access your account?`]: {
        pl: 'Nie możesz się zalogować?',
    },
    Pages: {
        pl: 'Strony',
    },
    Blog: {
        pl: 'Blog',
    },
    'Recent work': {
        pl: 'Ostatnie prace'
    },
    'Recently visited': {
        pl: 'Ostatnio oglądane'
    },
    'Recent spaces': {
        pl: 'Ostatnie przestrzenie'
    },
    'Kosmos intranet': {
        pl: 'Kosmos intranet'
    },
    'You do not have access to this page.': {
        pl: 'Nie masz dostępu do tej strony.'
    }
};

module.exports = (lang) => (phrase) => dictionary[phrase][lang] || phrase;
