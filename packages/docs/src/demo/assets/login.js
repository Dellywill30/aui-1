/* eslint-disable */

(function () {
    function checkAuthStatus() {
        const user = localStorage.getItem("user");

        if (user !== null) {
            return (window.location.href = document.body.dataset.homePage);
        }
    }

    window.addEventListener("load", checkAuthStatus);

    $("#log-in-form").on("submit", function (e) {
        e.preventDefault();

        const username = $("#log-in-username").val();

        localStorage.setItem("user", JSON.stringify({ username }));
        checkAuthStatus();
    });
})();
