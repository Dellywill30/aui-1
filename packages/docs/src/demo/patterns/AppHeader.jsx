const React = require('react');

const Avatar = require('../components/Avatar');
const Button = require('../components/Button');
const CloseButton = require('../components/CloseButton');

const SpacesDropdown = require('../partials/SpacesDropdown');
const RecentDropdown = require('../partials/RecentDropdown');

const SkipLinks = require('../partials/SkipLinks');
const ProfileDropdown = require('../partials/ProfileDropdown');
const DemoBanner = require('../partials/DemoBanner');

const MetalsmithContext = require('../../../build/MetalsmithContext');

const scriptContents = () => ({ __html: `
(function toggleDarkMode() {
    function darkness(val = null) {
        const storageKey = 'aui-theme';
        try {
            if (val != null) {
                localStorage.setItem(storageKey, !!val ? 'dark' : '');
            }
            return localStorage.getItem(storageKey) === 'dark';
        } catch (e) {
            console.error('No local storage access');
        }
        return false;
    }

    const darkModeClass = 'aui-theme-dark';
    document.body.classList.toggle(darkModeClass, darkness());
    const el = document.createElement('aui-toggle');
    el.id = 'switch-theme';
    el.setAttribute('label', 'Toggle dark mode');
    el.addEventListener('change', (e) => {
        const darkOn = e.target.checked;
        const result = darkness(darkOn);
        document.body.classList.toggle(darkModeClass, result);
    });
    document.querySelector('#switch-theme-container').appendChild(el);
}());
`});

module.exports = () => {
    const { menus, loggedIn, lang, translate } = React.useContext(
        MetalsmithContext
    );

    return loggedIn ? (
        <header id="header" role="banner">
            <SkipLinks />

            {/* <DemoBanner lang={lang} /> */}
            <nav
                class="aui-header aui-dropdown2-trigger-group"
                aria-label="site"
            >
                <div class="aui-header-inner">
                    <div class="aui-header-primary">
                        <span
                            id="logo"
                            class="aui-header-logo aui-header-logo-textonly"
                        >
                            <a href="#" aria-label="Go to home page">
                                <span class="aui-header-logo-device">
                                    Kosmos
                                </span>
                            </a>
                        </span>

                        <ul class="aui-nav">
                            <li>
                                <SpacesDropdown />
                            </li>
                            <li>
                                <RecentDropdown />
                            </li>
                            <li>
                                <a href="#">{translate('Questions')}</a>
                            </li>
                            <li>
                                <a href="#">{translate('Calendars')}</a>
                            </li>
                        </ul>

                        <div class="aui-buttons">
                            <Button
                                id="create-page--show-button"
                                variant="primary"
                            >
                                {translate('Create')}
                            </Button>
                        </div>

                        <section
                            id="create-page--dialog"
                            class="aui-dialog2 aui-dialog2-small aui-layer"
                            role="dialog"
                            tabindex="-1"
                            aria-labelledby="dialog-show-button--heading"
                            hidden
                        >
                            <header class="aui-dialog2-header">
                                <h1
                                    class="aui-dialog2-header-main"
                                    id="dialog-show-button--heading"
                                >
                                    {translate('Create page')}
                                </h1>
                                <CloseButton />
                            </header>
                            <div class="aui-dialog2-content">
                                <form
                                    class="aui top-label"
                                    method="post"
                                    id="create-page-form"
                                >
                                    <div class="field-group">
                                        <label for="create-page--space">
                                            {translate('Space')}
                                            <span class="aui-icon icon-required">
                                                {' '}
                                                {translate('required')}
                                            </span>
                                        </label>
                                        <select
                                            class="select"
                                            id="create-page--space"
                                            name="space"
                                            required
                                        >
                                            <option>
                                                {translate('Select')}
                                            </option>
                                            {menus.spaces.map(
                                                ({ id, label }) => (
                                                    <option key={id}>
                                                        {label}
                                                    </option>
                                                )
                                            )}
                                        </select>
                                    </div>
                                    <div class="field-group">
                                        <label for="create-page--title">
                                            {translate('Page title')}
                                            <span class="aui-icon icon-required">
                                                {' '}
                                                {translate('required')}
                                            </span>
                                        </label>
                                        <input
                                            required
                                            class="text"
                                            type="text"
                                            id="create-page--title"
                                            name="title"
                                        />
                                    </div>
                                </form>
                            </div>
                            <footer class="aui-dialog2-footer">
                                <div class="aui-dialog2-footer-actions">
                                    <Button
                                        id="create-page--cancel-button"
                                        class="aui-button"
                                    >
                                        {translate('Cancel')}
                                    </Button>
                                    <Button
                                        id="create-page--submit-button" // TODO make sure the id attribute is really needed here
                                        variant="primary"
                                        type="submit"
                                        form="create-page-form"
                                    >
                                        {translate('Create')}
                                    </Button>
                                </div>
                            </footer>
                        </section>
                    </div>
                    <div class="aui-header-secondary">
                        <ul class="aui-nav">
                            <li id="switch-theme-container">
                                <script dangerouslySetInnerHTML={scriptContents()} />
                            </li>
                            <li>
                                <a href="#" class="aui-nav-imagelink">
                                    <span class="aui-icon aui-icon-small aui-iconfont-notification">
                                        {translate('Notifications')}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="aui-nav-imagelink">
                                    <span class="aui-icon aui-icon-small aui-iconfont-help">
                                        {translate('Help')}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <ProfileDropdown>
                                    <Avatar />
                                </ProfileDropdown>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    ) : (
        <header id="header" role="banner">
            <SkipLinks loggedIn={loggedIn} />

            {/* <DemoBanner lang={lang} /> */}
            <nav
                class="aui-header aui-dropdown2-trigger-group"
                aria-label="site"
            >
                <div class="aui-header-inner">
                    <div class="aui-header-primary">
                        <h1
                            id="logo"
                            class="aui-header-logo aui-header-logo-textonly"
                        >
                            <a href="/demo">
                                <span class="aui-header-logo-device">
                                    Kosmos
                                </span>
                            </a>
                        </h1>

                        <ul class="aui-nav">
                            <li>
                                <SpacesDropdown loggedIn={loggedIn} />
                            </li>
                            <li>
                                <RecentDropdown />
                            </li>
                            <li>
                                <a href="#">{translate('Questions')}</a>
                            </li>
                            <li>
                                <a href="#">{translate('Calendars')}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="aui-header-secondary">
                        <ul class="aui-nav">
                            <li>
                                <a href="#" class="aui-nav-imagelink">
                                    <span class="aui-icon aui-icon-small aui-iconfont-notification">
                                        {translate('Notifications')}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="aui-nav-imagelink">
                                    <span class="aui-icon aui-icon-small aui-iconfont-help">
                                        {translate('Help')}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href={`/demo/${lang}/pages/login.html`}>
                                    {translate('Log in')}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );
};
