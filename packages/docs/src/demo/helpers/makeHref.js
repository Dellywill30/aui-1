/* eslint-disable */

module.exports = (id, loggedIn, lang) => `/demo/${lang}/pages/${id}/viewpage.action${loggedIn ? '' : '.logged-out'}.html`
