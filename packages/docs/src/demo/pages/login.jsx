const React = require('react');
const MetalsmithContext = require('../../../build/MetalsmithContext');

const Document = require('../partials/Document');
const PageFooter = require('../partials/PageFooter');
const DemoBanner = require('../partials/DemoBanner');

module.exports = () => {
    const { translate, lang } = React.useContext(MetalsmithContext);
    return (
        <Document
            title="Log In"
            mode="focused"
            size="medium"
            scripts={['/demo/assets/login.js']}
        >
            <a class="aui-skip-link" href="#main">
                {translate('Skip to main content')}
            </a>

            <div id="page">
                <header id="header" role="banner">
                    {/* <DemoBanner /> */}
                    <nav
                        class="aui-header aui-dropdown2-trigger-group"
                        aria-label="site"
                    >
                        <div class="aui-header-inner">
                            <div class="aui-header-primary">
                                <h1
                                    id="logo"
                                    class="aui-header-logo aui-header-logo-textonly"
                                >
                                    <a href="#">
                                        <span class="aui-header-logo-device">
                                            Kosmos
                                        </span>
                                    </a>
                                </h1>
                            </div>
                            <div class="aui-header-secondary">
                                <ul class="aui-nav">
                                    <li>
                                        <a href="#" class="aui-nav-imagelink">
                                            <span class="aui-icon aui-icon-small aui-iconfont-notification">
                                                {translate('Notifications')}
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="aui-nav-imagelink">
                                            <span class="aui-icon aui-icon-small aui-iconfont-help">
                                                {translate('Help')}
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href={`/demo/${lang}/pages/login.html`}>
                                            {translate('Log in')}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

                <main id="main" role="main">
                    <div class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <section class="aui-page-panel-content">
                                <h1>{translate('Welcome to intranet')}</h1>

                                <form
                                    class="aui"
                                    method="post"
                                    id="log-in-form"
                                >
                                    <div class="field-group">
                                        <label for="log-in-username">
                                            {translate('Username')}
                                        </label>
                                        <input
                                            class="text medium-field"
                                            type="text"
                                            id="log-in-username"
                                            name="log-in-username"
                                        />
                                    </div>
                                    <div class="field-group">
                                        <label for="log-in-password">
                                            {translate('Password')}
                                        </label>
                                        <input
                                            class="text medium-field"
                                            type="password"
                                            id="log-in-password"
                                            name="log-in-password"
                                        />
                                    </div>
                                    <div class="field-group">
                                        <div class="checkbox">
                                            <input
                                                class="checkbox"
                                                type="checkbox"
                                                name="checkBoxOne"
                                                id="checkBoxOne"
                                            />
                                            <label for="checkBoxOne">
                                                {translate(
                                                    'Remember my login on this computer'
                                                )}
                                            </label>
                                            <p>
                                                {translate(
                                                    'Not a member? To request an account, please contact your administrator.'
                                                )}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="buttons-container">
                                        <div class="buttons">
                                            <input // TODO can it be replaced by button tag? or maybe Button component?
                                                class="button submit"
                                                type="submit"
                                                value={translate("Log In")}
                                                id="log-in-submit-button"
                                            />
                                            <a class="cancel" href="#">
                                                {translate(
                                                    `Can't access your account?`
                                                )}
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </main>

                <PageFooter />
            </div>
        </Document>
    );
};
