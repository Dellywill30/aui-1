/// <reference types="Cypress" />
/* eslint-disable */

const variants = {
    'experimental/avatar/sizes/': ['#content'],
    'auiBadge/': ['#content'],
    'experimental/buttons/': ['#content'],
    'experimental/datePicker/': [{
        capture: '#date-picker10',
        suffix: () => '.aui-datepicker-dialog[aria-hidden=false]',
        setup: () => cy.get('#test-default-always').click(),
    }],
    'i18n/fontStacks/': ['#content'],
    'icons/': ['#content'],
    'inlineDialog2/': [{
        capture: 'body',
        suffix: (selector) => selector + '_onload'
    }, {
        capture: 'body',
        setup: () => {
            // persistent dialog
            cy.get('[aria-controls="inline-dialog2-18"]').click();
            // inside large text
            cy.get('[aria-controls="aui-5144-dialog"]').click();
        },
        suffix: (selector) => selector + '_variants'
    }],
    'experimental/lozenges/': ['#content'],
    'experimental/pageLayout/layouts/navigation/default/': ['#content'],
    'experimental/pageLayout/layouts/groups/': ['#content'],
    'experimental/pageLayout/header/auiHeader/': ['#nav1', '#nav2', '#nav3', '#nav4'],
    'experimental/pageLayout/header/pageHeaderVariations/': ['#content'],
    'tables/': ['#basic', '#nested'],
    'experimental/toolbar2/': ['#content']
};

Object.entries(variants).forEach(([path, selectors]) => {
    context(path, () => {
        const pathPrefix = path.replace(/\//g, '_');

        beforeEach(() => {
            cy.visit(path);
            cy.document().then((document) => {
                const el = document.createElement('style');
                el.innerHTML = `html { caret-color: transparent !important; }`;
                document.head.appendChild(el);
            });
        });

        selectors.forEach(val => {
            if (typeof val === 'string') {
                val = { capture: val };
            }
            const {
                capture,
                setup = () => true,
                suffix = (selector) => selector
            } = val;

            it(capture, () => {
                setup();
                cy.get(capture).matchImageSnapshot(`${pathPrefix}_${suffix(capture)}`)
            })
        });
    });
});
